// START A3HEADER
//
// This source file is part of the Atlantis PBM game program.
// Copyright (C) 1995-1999 Geoff Dunbar
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program, in the file license.txt. If not, write
// to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
// Boston, MA 02111-1307, USA.
//
// See the Atlantis Project web page for details:
// http://www.prankster.com/project
//
// END A3HEADER
#ifndef RULES_H
#define RULES_H

//
// The items
//
// xxxxx - In an ideal world, this wouldn't be necessary.
//
#define I_LEADERS -1
#define I_GRAIN -1
#define I_LIVESTOCK -1
#define I_FISH -1
#define I_AMULETOFI -1
#define I_WAGON -1

enum {
    I_MAN,
    I_SILVER,
    I_IRON,
    I_WOOD,
    I_STONE,
    I_HORSE,
    I_SWORD,
    I_CROSSBOW,
    I_LONGBOW,
    I_CHAINARMOR,
    I_PLATEARMOR,
    I_STAFF_OF_LIGHT,
    I_STAFF_OF_FEAR,
    I_STAFF_OF_FIRE,
    I_AMULET_OF_INVISIBILITY,
    I_AMULET_OF_TRUE_SEEING,
    I_PEASANT,
    NITEMS
};

//
// Types of men.
//
enum {
    MAN_NONE,
    MAN_MAN,
    NUMMAN
};

//
// Types of monsters
//
#define MONSTER_ILLUSION -1

enum {
    MONSTER_NONE,
    NUMMONSTERS
};

//
// Types of weapons
//
enum {
    WEAPON_NONE,
    WEAPON_LONGBOW,
    WEAPON_CROSSBOW,
    WEAPON_SWORD,
    NUMWEAPONS
};

//
// Types of armor
//
enum {
    ARMOR_NONE,
    ARMOR_PLATEARMOR,
    ARMOR_CHAINARMOR,
    NUMARMORS
};

//
// Types of mounts
//
enum {
    MOUNT_NONE,
    MOUNT_HORSE,
    NUMMOUNTS
};

//
// Other battle items
//
enum {
    BATTLE_NONE,
    BATTLE_STAFF_OF_LIGHT,
    BATTLE_STAFF_OF_FEAR,
    BATTLE_STAFF_OF_FIRE,
    NUMBATTLEITEMS,
};

//
// Types of skills.
//
// xxxxx - Again, in an ideal world, this wouldn't be necessary
//
#define S_GATE_LORE -1
#define S_MIND_READING -1
#define S_SUMMON_WIND -1
#define S_ILLUSION -1
#define S_PHANTASMAL_ENTERTAINMENT -1

enum {
    S_MINING,
    S_LUMBERJACK,
    S_QUARRYING,
    S_HORSETRAINING,
    S_WEAPONSMITH,
    S_ARMORER,
    S_BUILDING,
    S_SHIPBUILDING,
    S_ENTERTAINMENT,
    S_TACTICS,
    S_COMBAT,
    S_RIDING,
    S_CROSSBOW,
    S_LONGBOW,
    S_STEALTH,
    S_OBSERVATION,
    S_SAILING,
    S_MAGIC,
    S_DAZZLE,
    S_LIGHTNING_BOLT,
    S_CAUSE_FEAR,
    S_INSPIRE_COURAGE,
    S_FIREBALL,
    S_CREATE_STAFF_OF_LIGHT,
    S_INVISIBILITY,
    S_TRUE_SEEING,
    S_CREATE_STAFF_OF_FEAR,
    S_TELEPORT,
    S_BLACK_WIND,
    S_CREATE_STAFF_OF_FIRE,
    S_CREATE_AMULET_OF_INVISIBILITY,
    S_CREATE_AMULET_OF_TRUE_SEEING,
    NSKILLS
};

//
// Types of special attacks
//
#define SPECIAL_EARTHQUAKE -1

enum {
    SPECIAL_NONE_DUMMY,
    SPECIAL_DAZZLE,
    SPECIAL_LIGHTNING_BOLT,
    SPECIAL_CAUSE_FEAR,
    SPECIAL_INSPIRE_COURAGE,
    SPECIAL_FIREBALL,
    SPECIAL_BLACK_WIND,
    NUMSPECIALS
};

//
// Types of objects.
//
// xxxxx
//
#define O_SHAFT -1
#define O_BALLOON -1

enum {
    O_DUMMY,
    O_LONGBOAT,
    O_CLIPPER,
    O_GALLEON,
    O_TOWER,
    O_FORT,
    O_KEEP,
    O_CASTLE,
    O_CITADEL,
    NOBJECTS
};

//
// Types of terrain
//
// xxxxx - Once again, in an ideal world, this wouldn't be necessary
//
#define R_NEXUS -1

enum {
    R_OCEAN,
    R_PLAIN,
    R_FOREST,
    R_MOUNTAIN,
    R_SWAMP,
    R_NUM
};


#endif
