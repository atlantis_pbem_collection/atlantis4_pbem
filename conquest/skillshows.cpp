// START A3HEADER
//
// This source file is part of the Atlantis PBM game program.
// Copyright (C) 1995-1999 Geoff Dunbar
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program, in the file license.txt. If not, write
// to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
// Boston, MA 02111-1307, USA.
//
// See the Atlantis Project web page for details:
// http://www.prankster.com/project
//
// END A3HEADER
#include "skills.h"
#include "rules.h"

static ShowType sd[] =
{
    /* skill, level, desc */

    { S_MINING, 1,
      "A unit with this skill may use the PRODUCE order to produce iron in "
      "designated regions. Iron is found primarily in mountain regions, but "
      "may also be found in other regions. The Weaponsmith skill is "
      "used to forge iron into weapons." },
    { S_LUMBERJACK, 1,
      "A unit with "
      "this skill may use the PRODUCE order to produce wood in "
      "designated regions. Forest regions are generally the best "
      "producers of wood, though other regions may also produce wood. "
      "Wood may be used to construct boats, using the Shipbuilding "
      "skill, or bows, using the Weaponsmith skill." },
    { S_QUARRYING, 1,
      "A unit with "
      "this skill may use the PRODUCE order to produce stone in "
      "designated regions. Mountains are the main producers of stone, "
      "though other regions may also produce stone. Stone may be used "
      "to construct buildings, using the Building skill." },
    { S_HORSETRAINING, 1,
      "A unit with "
      "this skill may use the PRODUCE order to produce horses in "
      "designated regions. Horses are found in plains regions." },
    { S_WEAPONSMITH, 1,
      "A unit with "
      "this skill may use the PRODUCE order to produce weapons from "
      "certain items. Swords may be produced from iron, and crossbows "
      "and longbows may be produced from wood." },
    { S_ARMORER, 1,
      "A unit with this "
      "skill may PRODUCE chain armor from iron. Chain armor provides a "
      "1/3 chance of surviving a successful attack in battle." },
    { S_ARMORER, 3,
      "A unit with this "
      "skill may PRODUCE plate armor from 3 units of iron. Plate armor "
      "provides a 2/3 chance of surviving a successful attack in "
      "battle." },
    { S_BUILDING, 1,
      "A unit with this "
      "skill may use the BUILD order to construct buildings from "
      "stone." },
    { S_SHIPBUILDING, 1,
      "A unit with "
      "this skill may use the BUILD order to construct ships from "
      "wood." },
    { S_ENTERTAINMENT, 1,
      "A unit with "
      "this skill may use the ENTERTAIN order to generate funds. The "
      "amount of silver gained will be 20 per man, times the level of "
      "the entertainers. This amount is limited by the region that "
      "the unit is in." },
    { S_TACTICS, 1,
      "Tactics allows the "
      "unit, and all allies, to gain a free round of attacks during "
      "battle. The army with the highest level tactician in a battle "
      "will receive this free round; if the highest levels are equal, "
      "no free round is awarded." },
    { S_COMBAT, 1,
      "This skill gives "
      "the unit a bonus in hand to hand combat. Also, a unit with this "
      "skill may TAX or PILLAGE." },
    { S_RIDING, 1,
      "A unit with this "
      "skill, if possessing a horse, gains a bonus in combat if the "
      "battle is in a plain, desert, or tundra. This bonus "
      "is equal to the skill level, up to a maximum of 2." },
    { S_CROSSBOW, 1,
      "A unit with "
      "this skill may use a crossbow, either in battle, or to TAX "
      "or PILLAGE a region." },
    { S_LONGBOW, 1,
      "A unit with "
      "this skill may use a longbow, either in battle, or to TAX "
      "or PILLAGE a region." },
    { S_STEALTH, 1,
      "A unit with this "
      "skill is concealed from being seen, except by units with an "
      "Observation skill greater than or equal to the stealthy unit's "
      "Stealth level." },
    { S_OBSERVATION, 1,
      "A unit with "
      "this skill can see stealthy units whose stealth rating is less "
      "than or equal to the observing unit's Observation level. The "
      "unit can also determine the faction owning a unit, provided its "
      "Observation level is higher than the other unit's Stealth "
      "level." },
    { S_SAILING, 1,
      "A unit with this "
      "skill may use the SAIL order to sail ships." },
    { S_DAZZLE, 1,
      "This spell allows the casting mage to dazzle from 2 to 50 times his "
      "skill level enemy soldiers per round of combat. Dazzled soldiers "
      "attack at -2 on their next attack." },
    { S_LIGHTNING_BOLT, 1,
      "This spell allows the casting mage to shoot a lightning bolt against "
      "an enemy army in combat. A lightning bolt will kill from 2 to 10 "
      "times the mage's skill level soldiers each round it is cast." },
    { S_CAUSE_FEAR, 1,
      "This spell allows the casting mage to summon fearful illusions "
      "which decrease enemy soldiers' effectiveness. The mage may "
      "frighten from 2 to 50 times his skill level soldiers per round, "
      "in combat. A frightened soldier attacks and defends at -2 for the "
      "duration of a battle." },
    { S_INSPIRE_COURAGE, 1,
      "This spell allows the casting mage to inspire courage in soldiers, "
      "removing the effects of the Cause Fear spell. Each round that this "
      "spell is cast, from 2 to 50 friendly soldiers may be inspired." },
    { S_FIREBALL, 1,
      "This spell allows the casting mage to summon a ball of fire against "
      "an opposing army. Each round that this spell is cast, it will slay "
      "from 2 to 50 times the mage's skill level enemies." },
    { S_CREATE_STAFF_OF_LIGHT, 1,
      "This spell allows the mage to create a Staff of Light. When the "
      "mage casts this spell, he has a 20% times his skill level chance "
      "of successfully creating a Staff of Light; the attempt costs 200 "
      "silver whether it succeeds or not. A Staff of Light allows a mage "
      "to cast the Dazzle spell, at level 3, even if he does not possess "
      "the spell." },
    { S_INVISIBILITY, 1,
      "This spell allows the mage to render himself invisible, giving "
      "himself a Stealth bonus equal to his skill level. This spell is "
      "used automatically; no order is needed to get the Stealth bonus." },
    { S_TRUE_SEEING, 1,
      "This spell grants the mage extraordinary powers of perception. "
      "This gives him an Observation bonus equal to his skill level. "
      "No order is needed to use this spell; the Observation bonus is "
      "automatic." },
    { S_CREATE_STAFF_OF_FEAR, 1,
      "This spell allows the mage to create a Staff of Fear. When the "
      "mage casts this spell, he has a 20% times his skill level chance "
      "of successfully creating a Staff of Fear; the attempt costs 400 "
      "silver whether it succeeds or not. A Staff of Fear allows a mage "
      "to cast the Cause Fear spell, at level 3, even if he does not "
      "possess the spell." },
    { S_TELEPORT, 1,
      "This spell allows the caster to move himself and others across vast "
      "distances without traversing the intervening space. The command "
      "to use it is CAST TELEPORT target-unit UNITS unit-no ... The target "
      "unit is a unit in the region to which the teleport is to occur, "
      "and must also possess the Teleport spell. If the target unit is "
      "not in your faction, the target's faction must be declared Friendly "
      "or better towards the casting unit's faction. After the target unit "
      "comes a list of one or more units to be teleported into the target "
      "unit's region (this may optionally include the caster). Any units "
      "to be teleported, not in your faction, must be in a faction which "
      "has declared the casting unit's faction Friendly or better. The "
      "total weight of all units to be teleported (including people, "
      "equipment and horses) must not exceed 1000 weight units, and the "
      "maximum distance between the source region and the target region "
      "can be at most 5 times the casting mage's skill level.  If the "
      "target unit is in a building or on a ship, the teleported units "
      "will emerge there, regardless of who owns the building or ship. "
      "This spell occurs at the end of the turn (during the Teleportation "
      "orders phase), so the units to be transported can spend the month "
      "doing something else." },
    { S_BLACK_WIND, 1,
      "This spell creates a black whirlwind of energy which destroys all "
      "life, leaving frozen corpses with faces twisted into expressions "
      "of horror. Cast in battle, it kills from 2 to 250 times the mage's "
      "skill level enemies, per round the spell is cast." },
    { S_CREATE_STAFF_OF_FIRE, 1,
      "This spell allows the mage to create a Staff of Fire. When the mage "
      "casts this spell, he has a 20% times his skill level chance of "
      "successfully creating a Staff of Fire; the attempt costs 600 "
      "silver whether it succeeds or not. A Staff of Fire allows a mage "
      "to cast the Fireball spell, at level 3, even if he does not "
      "possess the spell." },
    { S_CREATE_AMULET_OF_INVISIBILITY, 1,
      "This spell allows the mage to create an Amulet of Invisibility. "
      "When the mage casts this spell, he has a 20% times his skill level "
      "chance of successfully creating an Amulet of Invisibility; the "
      "attempt costs 600 silver whether it succeeds or not. An Amulet of "
      "Invisibility gives a single-man unit a +3 Stealth bonus." },
    { S_CREATE_AMULET_OF_TRUE_SEEING, 1,
      "This spell allows the mage to create an Amulet of True Seeing. When "
      "the mage casts this spell, he has a 20% times his skill level chance "
      "of successfully creating an Amulet of True Seeing; the attempt "
      "costs 600 silver whether it succeeds or not. An Amulet of True "
      "Seeing gives a unit a +3 Observation bonus." },
    { -1, -1, ""}
};

ShowType * ShowDefs = sd;
	      
