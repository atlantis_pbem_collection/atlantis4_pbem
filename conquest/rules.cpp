// START A3HEADER
//
// This source file is part of the Atlantis PBM game program.
// Copyright (C) 1995-1999 Geoff Dunbar
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program, in the file license.txt. If not, write
// to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
// Boston, MA 02111-1307, USA.
//
// See the Atlantis Project web page for details:
// http://www.prankster.com/project
//
// END A3HEADER
#include "rules.h"
#include "items.h"
#include "skills.h"
#include "object.h"
#include "aregion.h"

//
// Define the various globals for this game.
//
static GameDefs g = {
    "Atlantis Conquest", // RULESET_NAME
    MAKE_ATL_VER( 1, 0, 4 ), // RULESET_VERSION
    MAKE_ATL_VER( 4, 0, 4 ), // ENGINE_VERSION

    2, /* FOOT_SPEED */
    4, /* HORSE_SPEED */
    4, /* SHIP_SPEED */
    6, /* FLY_SPEED */
    8, /* MAX_SPEED */
		   
    250, /* WAGON_CAPACITY */

    10, /* STUDENTS_PER_TEACHER */
    10, /* MAINTENANCE_COST */
    10, /* LEADER_COST */
    33, /* STARVE_PERCENT */
    5020, /* START_MONEY */
    5, /* WORK_FRACTION */
    20, /* ENTERTAIN_FRACTION */
    20, /* ENTERTAIN_INCOME */

    50, /* TAX_INCOME */
	
    5, /* HEALS_PER_MAN */

    20, /* GUARD_REGEN */ /* percent */
    40, /* CITY_GUARD */
    50, /* GUARD_MONEY */
    4000, /* CITY_POP */

    10, /* WMON_FREQUENCY */
    10, /* LAIR_FREQUENCY */
	
    5, /* FACTION_POINTS */

    64, /* MAX_AC_X */
    64, /* MAX_AC_Y */

    100, /* TURN_COST */

    50, /* TIMES_REWARD */

    0, // TOWNS_EXIST
    0, // LEADERS_EXIST
    0, // SKILL_LIMIT_NONLEADERS
    1, // MAGE_NONLEADERS
    0, // RACES_EXIST
    0, // GATES_EXIST
    0, // FOOD_ITEMS_EXIST
    0, // CITY_MONSTERS_EXIST
    0, // WANDERING_MONSTERS_EXIST
    0, // LAIR_MONSTERS_EXIST
    0, // WEATHER_EXISTS
    0, // OPEN_ENDED

    0, // RANDOM_ECONOMY
    0, // VARIABLE_ECONOMY

    0, // CITY_MARKET_NORMAL_AMT
    0, // CITY_MARKET_ADVANCED_AMT
    0, // CITY_MARKET_TRADE_AMT

    GameDefs::FACLIM_MAGE_COUNT, // FACTION_LIMIT_TYPE
};

GameDefs * Globals = &g;

//
// Table of items
//
// name, plural,abbr, flags, skill, level, input, number, weight, type,
// base price, combat, monster, walk,ride,fly,swim
//
ItemType id[] =
{
    {"man","men","MAN",0,-1,0,-1,0,10,IT_MAN,
     50,1,MAN_MAN,15,0,0,0},
    {"silver","silver","SILV",0,-1,0,-1,0,0,IT_NORMAL,
     1,0,0,0,0,0,0},
    {"iron","iron","IRON",0,S_MINING,1,-1,0,5,IT_NORMAL,
     30,0,0,0,0,0,0},
    {"wood","wood","WOOD",0,S_LUMBERJACK,1,-1,0,5,IT_NORMAL,
     30,0,0,0,0,0,0},
    {"stone","stone","STON",0,S_QUARRYING,1,-1,0,50,IT_NORMAL,
     30,0,0,0,0,0,0},
    {"horse","horses","HORS",0,S_HORSETRAINING,1,-1,0,50,
     IT_NORMAL | IT_MOUNT,
     30,1,MOUNT_HORSE,70,70,0,0},
    {"sword","swords","SWOR",0,S_WEAPONSMITH,1,I_IRON,1,1,
     IT_NORMAL | IT_WEAPON,
     60,1,WEAPON_SWORD,0,0,0,0},
    {"crossbow","crossbows","XBOW",0,S_WEAPONSMITH,1,I_WOOD,1,1,
     IT_NORMAL | IT_WEAPON,
     60,1,WEAPON_CROSSBOW,0,0,0,0},
    {"longbow","longbows","LBOW",0,S_WEAPONSMITH,1,I_WOOD,1,1,
     IT_NORMAL | IT_WEAPON,
     60,1,WEAPON_LONGBOW,0,0,0,0},
    {"chain armor","chain armor","CARM",0,S_ARMORER,1,I_IRON,1,1,
     IT_NORMAL | IT_ARMOR,
     60,1,ARMOR_CHAINARMOR,0,0,0,0},
    {"plate armor","plate armor","PARM",0,S_ARMORER,3,I_IRON,3,3,
     IT_NORMAL | IT_ARMOR,
     250,1,ARMOR_PLATEARMOR,0,0,0,0},
    {"staff of light","staves of light","STLI",0,
     -1,0,-1,0,1,IT_MAGIC | IT_BATTLE,
     1000,1,BATTLE_STAFF_OF_LIGHT,0,0,0,0},
    {"staff of fear","staves of fear","STFE",0,
     -1,0,-1,0,1,IT_MAGIC | IT_BATTLE,
     1000,1,BATTLE_STAFF_OF_FEAR,0,0,0,0},
    {"staff of fire","staves of fire","STFI",0,-1,0,-1,0,1,
     IT_MAGIC | IT_BATTLE,
     1000,1,BATTLE_STAFF_OF_FIRE,0,0,0,0},
    {"amulet of invisibility","amulets of invisibility","AMIN",0,
     -1,0,-1,0,0,IT_MAGIC,
     1000,0,0,0,0,0,0},
    {"amulet of true seeing","amulets of true seeing","AMTS",0,
     -1,0,-1,0,0,IT_MAGIC,
     1000,0,0,0,0,0,0},
    {"peasant","peasants","PEAS",0,
     -1,0,-1,0,10,IT_MAN,
     50,1,0,15,0,0,0},
};

ItemType * ItemDefs = id;

//
// Table of men
//
ManType mt[] = {
    //
    // Special level, default, 1st skill, 2nd, 3rd, 4th
    //
    { 0,0,0,0,0,0 }, /*  MAN_NONE */
    { 5,5,-1,-1,-1,-1 }, /* MAN_MAN */
};

ManType * ManDefs = mt;

//
// Table of monsters.
//
MonType md[] = {
    //
    // skill,hits,tactics,special,stealth,obs,
    // silver,spoiltype,hostile,number,name
    //
    {0,0,0,-1,0,0,
     0,-1,0,0,"None"}, /* none */
    /* skill,hits,tactics,special,stealth,obs,
       silver,spoiltype,hostile,number,name */
};

MonType * MonDefs = md;

//
// Table of weapons.
//
WeaponType wepd[] = {
    // WEAPON_NONE
    { -1, -1, 0, 0, 0 },
    // WEAPON_LONGBOW
    { S_LONGBOW, -1,
      WeaponType::NEEDSKILL | WeaponType::RANGED | WeaponType::NODEFENSE,
      -2, 1 },
    // WEAPON_CROSSBOW
    { S_CROSSBOW, -1,
      WeaponType::NEEDSKILL | WeaponType::RANGED | WeaponType::NODEFENSE |
      WeaponType::GOODARMOR,
      0, -2 },
    // WEAPON_SWORD
    { -1, -1, 0, 2, 1 },
};

WeaponType *WeaponDefs = wepd;

//
// Table of armors.
//
ArmorType armd[] = {
    // ARMOR_NONE
    { 0, 0, 0, 0, 0 },
    // ARMOR_PLATEARMOR
    { 0, 2, 3, 2, 3 },
    // ARMOR_CHAINARMOR
    { 0, 1, 3, 1, 3 },
};

ArmorType *ArmorDefs = armd;

//
// Table of mounts
//
MountType mountd[] = {
    // MOUNT_NONE
    { 0, 0, 0 },
    // MOUNT_HORSE
    { S_RIDING, 1, 3 },
};

MountType *MountDefs = mountd;

//
// Table of other battle items
//
BattleItemType bitd[] = {
    // BATTLE_NONE
    { 0, 0, 0, 0 },
    // BATTLE_STAFF_OF_LIGHT,
    { BattleItemType::MAGEONLY | BattleItemType::SPECIAL,
      I_STAFF_OF_LIGHT, SPECIAL_DAZZLE, 3 },
    // BATTLE_STAFF_OF_FEAR,
    { BattleItemType::MAGEONLY | BattleItemType::SPECIAL,
      I_STAFF_OF_FEAR, SPECIAL_CAUSE_FEAR, 3 },
    // BATTLE_STAFF_OF_FIRE,
    { BattleItemType::MAGEONLY | BattleItemType::SPECIAL,
      I_STAFF_OF_FIRE, SPECIAL_FIREBALL, 3 },
};

BattleItemType *BattleItemDefs = bitd;

//
// Table of skills.
//
static SkillType sd[] = {
    //
    // name, abbr, cost, flags, special
    // depend1, level1, depend2, level2, depend3, level3
    //
    {"mining","MINI",10,0,0,-1,0,-1,0,-1,0},
    {"lumberjack","LUMB",10,0,0,-1,0,-1,0,-1,0},
    {"quarrying","QUAR",10,0,0,-1,0,-1,0,-1,0},
    {"horse training","HORS",10,0,0,-1,0,-1,0,-1,0},
    {"weaponsmith","WEAP",10,0,0,-1,0,-1,0,-1,0},
    {"armorer","ARMO",10,0,0,-1,0,-1,0,-1,0},
    {"building","BUIL",10,0,0,-1,0,-1,0,-1,0},
    {"shipbuilding","SHIP",10,0,0,-1,0,-1,0,-1,0},
    {"entertainment","ENTE",10,0,0,-1,0,-1,0,-1,0},
    {"tactics","TACT",200,0,0,-1,0,-1,0,-1,0},
    {"combat","COMB",10,0,0,-1,0,-1,0,-1,0},
    {"riding","RIDI",10,0,0,-1,0,-1,0,-1,0},
    {"crossbow","XBOW",10,0,0,-1,0,-1,0,-1,0},
    {"longbow","LBOW",10,0,0,-1,0,-1,0,-1,0},
    {"stealth","STEA",50,0,0,-1,0,-1,0,-1,0},
    {"observation","OBSE",50,0,0,-1,0,-1,0,-1,0},
    {"sailing","SAIL",10,0,0,-1,0,-1,0,-1,0},
    {"magic","MAGI",100,SkillType::MAGIC | SkillType::FOUNDATION,
     0,-1,0,-1,0,-1,0},
    {"dazzle","DAZZ",100,SkillType::MAGIC | SkillType::COMBAT,
     SPECIAL_DAZZLE,S_MAGIC,1,-1,0,-1,0},
    {"lightning bolt","LIGH",100,SkillType::MAGIC | SkillType::COMBAT,
     SPECIAL_LIGHTNING_BOLT,S_MAGIC,1,-1,0,-1,0},
    {"cause fear","FEAR",100,SkillType::MAGIC | SkillType::COMBAT,
     SPECIAL_CAUSE_FEAR,S_MAGIC,2,-1,0,-1,0},
    {"inspire courage","COUR",100,SkillType::MAGIC | SkillType::COMBAT,
     SPECIAL_INSPIRE_COURAGE,S_MAGIC,2,-1,0,-1,0},
    {"fireball","FIRE",100,SkillType::MAGIC | SkillType::COMBAT,
     SPECIAL_FIREBALL,S_MAGIC,3,-1,0,-1,0},
    {"create staff of light","CSLI",100,SkillType::MAGIC | SkillType::CAST,
     0,S_MAGIC,3,-1,0,-1,0},
    {"invisibility","INVI",100,SkillType::MAGIC,
     0,S_MAGIC,3,-1,0,-1,0},
    {"true seeing","TRSE",100,SkillType::MAGIC,
     0,S_MAGIC,3,-1,0,-1,0},
    {"create staff of fear","CSFE",100,SkillType::MAGIC | SkillType::CAST,
     0,S_MAGIC,4,-1,0,-1,0},
    {"teleport","TELE",100,SkillType::MAGIC | SkillType::CAST,
     0,S_MAGIC,4,-1,0,-1,0},
    {"black wind","BLWI",100,SkillType::MAGIC | SkillType::COMBAT,
     SPECIAL_BLACK_WIND,S_MAGIC,5,-1,0,-1,0},
    {"create staff of fire","CSFI",100,SkillType::MAGIC | SkillType::CAST,
     0,S_MAGIC,5,-1,0,-1,0},
    {"create amulet of invisibility","CAIN",100,
     SkillType::MAGIC | SkillType::CAST,
     0,S_MAGIC,5,-1,0,-1,0},
    {"create amulet of true seeing","CSTS",100,
     SkillType::MAGIC | SkillType::CAST,
     0,S_MAGIC,5,-1,0,-1,0},
};

SkillType * SkillDefs = sd;

//
// Table of objects.
//
static ObjectType ot[] =
{
    //
    // name,protect,capacity,item,cost,level,skill,sailors,canenter,monster,
    // production aided.
    //
    {"None",0,0,-1,-1,0,-1,0,1,-1,-1},
    {"Longboat",0,200,I_WOOD,25,1,S_SHIPBUILDING,5,1,-1,-1},
    {"Clipper",0,800,I_WOOD,50,1,S_SHIPBUILDING,10,1,-1,-1},
    {"Galleon",0,1800,I_WOOD,75,1,S_SHIPBUILDING,15,1,-1,-1},
    {"Tower",10,0,I_STONE,10,1,S_BUILDING,0,1,-1,-1},
    {"Fort",50,0,I_STONE,40,1,S_BUILDING,0,1,-1,-1},
    {"Keep",250,0,I_STONE,160,1,S_BUILDING,0,1,-1,-1},
    {"Castle",1250,0,I_STONE,640,1,S_BUILDING,0,1,-1,-1},
    {"Citadel",1250,0,-1,-1,0,-1,0,1,-1,-1},
};

ObjectType * ObjectDefs = ot;

//
// Table of terrain types.
//
static TerrainType td[] = {
    //
    // name, flags,
    // pop, wages, economy, movepoints, prod1, chance1, amt1,
    // prod2, chance2, amt2, prod3, chance3, amt3,
    // prod4, chance4, amt4, prod5, chance5, amt5,
    // race1, race2, race3, coastalrace1, coastalrace2,
    // wmonfreq, smallmon, bigmon, humanoid,
    // lairChance, lair1, lair2, lair3, lair4
    //
    {"ocean",0,
     0,0,0,1,-1,0,0,
     -1,0,0,-1,0,0,
     -1,0,0,-1,0,0,
     -1,-1,-1,-1,-1,
     0,-1,-1,-1,
     0,-1,-1,-1,-1},
    {"plain", TerrainType::RIDINGMOUNTS | TerrainType::FLYINGMOUNTS,
     800,14,40,1,I_HORSE,100,20,
     -1,0,0,-1,0,0,
     -1,0,0,-1,0,0,
     I_MAN,-1,-1,-1,-1,
     0,-1,-1,-1,
     0,-1,-1,-1,-1},
    {"forest", TerrainType::FLYINGMOUNTS,
     400,12,20,2,I_WOOD,100,20,
     -1,0,0,-1,0,0,
     -1,0,0,-1,0,0,
     I_MAN,-1,-1,-1,-1,
     0,-1,-1,-1,
     0,-1,-1,-1,-1},
    {"mountain", TerrainType::FLYINGMOUNTS,
     400,12,20,2,I_IRON,100,20,
     I_STONE,100,10,-1,0,0,
     -1,0,0,-1,0,0,
     I_MAN,-1,-1,-1,-1,
     0,-1,-1,-1,
     0,-1,-1,-1,-1},
    {"swamp", TerrainType::FLYINGMOUNTS,
     200,11,10,2,I_WOOD,100,10,
     -1,0,0,-1,0,0,
     -1,0,0,-1,0,0,
     I_MAN,-1,-1,-1,-1,
     0,-1,-1,-1,
     0,-1,-1,-1,-1},
};

TerrainType * TerrainDefs = td;

