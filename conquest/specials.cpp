// START A3HEADER
//
// This source file is part of the Atlantis PBM game program.
// Copyright (C) 1995-1999 Geoff Dunbar
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program, in the file license.txt. If not, write
// to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
// Boston, MA 02111-1307, USA.
//
// See the Atlantis Project web page for details:
// http://www.prankster.com/project
//
// END A3HEADER
#include "battle.h"
#include "rules.h"

void Soldier::SetupHealing()
{
    //
    // No healing
    //
}

int Army::CheckSpecialTarget(int special,int tar)
{
    switch( special )
    {
    case SPECIAL_DAZZLE:
        if( soldiers[ tar ]->HasEffect( EFFECT_DAZZLE )) return 0;
        break;

    case SPECIAL_CAUSE_FEAR:
        if( soldiers[ tar ]->HasEffect( EFFECT_FEAR )) return 0;
        break;

    case SPECIAL_LIGHTNING_BOLT:
    case SPECIAL_FIREBALL:
    case SPECIAL_BLACK_WIND:
        break;
    }

    return 1;
}

void Battle::UpdateShields(Army *a)
{
    //
    // No Shields.
    //
}

void Battle::DoSpecialAttack( int round,
                              Soldier *a,
                              Army *attackers,
                              Army *def, 
                              int behind )
{
    int num;
    switch( a->special )
    {
    case -1:
        break;

    case SPECIAL_DAZZLE:
        num = def->DoAnAttack( a->special,
                               getrandom( a->slevel * 25 ) +
                               getrandom( a->slevel * 25 ) + 2,
                               ATTACK_ENERGY,
                               a->slevel,
                               SPECIAL_FLAGS,
                               EFFECT_DAZZLE );
        AddLine( *(a->unit->name) + " creates a brilliant flash of light, "
                 "dazzling " + num + " troops.");
        break;

    case SPECIAL_LIGHTNING_BOLT:
        num = def->DoAnAttack( a->special,
                               getrandom( a->slevel * 5 ) +
                               getrandom( a->slevel * 5 ) + 2,
                               ATTACK_ENERGY,
                               a->slevel,
                               SPECIAL_FLAGS,
                               0 );
        AddLine( *(a->unit->name) + " unleashes a bolt of lightning, "
                 "killing " + num + " troops.");
        break;

    case SPECIAL_CAUSE_FEAR:
        num = def->DoAnAttack( a->special,
                               getrandom( a->slevel * 25 ) +
                               getrandom( a->slevel * 25 ) + 2,
                               ATTACK_SPIRIT,
                               a->slevel,
                               SPECIAL_FLAGS,
                               EFFECT_FEAR );
        AddLine( *(a->unit->name) + " strikes fear into the hearts of " +
                 num + " men.");
        break;

    case SPECIAL_INSPIRE_COURAGE:
        num = attackers->RemoveEffects( getrandom( a->slevel * 25 ) +
                                        getrandom( a->slevel * 25 ) + 2,
                                        EFFECT_FEAR );
        AddLine( *(a->unit->name) + " inspires courage in the hearts of " +
                 num + " men." );
        break;

    case SPECIAL_FIREBALL:
        num = def->DoAnAttack( a->special,
                               getrandom( a->slevel * 25 ) +
                               getrandom( a->slevel * 25 ) + 2,
                               ATTACK_ENERGY,
                               a->slevel,
                               SPECIAL_FLAGS,
                               0 );
        AddLine( *(a->unit->name) + " shoots a Fireball, "
                 "incinerating " + num + " troops.");
        break;

    case SPECIAL_BLACK_WIND:
        num = def->DoAnAttack( a->special,
                               getrandom( a->slevel * 125 ) +
                               getrandom( a->slevel * 125 ) + 2,
                               ATTACK_SPIRIT,
                               a->slevel,
                               SPECIAL_FLAGS,
                               0 );
        AddLine( *(a->unit->name) + " sends a black whirlwind across the "
                 "battlefield, killing " + num + " troops.");
        break;

    default:
        break;
    }
}

