// START A3HEADER
//
// This source file is part of the Atlantis PBM game program.
// Copyright (C) 1995-1999 Geoff Dunbar
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program, in the file license.txt. If not, write
// to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
// Boston, MA 02111-1307, USA.
//
// See the Atlantis Project web page for details:
// http://www.prankster.com/project
//
// END A3HEADER
#include "game.h"
#include "rules.h"

static char *regionnames[] =
{
    "Aathon",
    "Aberaeron",
    "Aberdaron",
    "Aberdovey",
    "Abernecht",
    "Abernethy",
    "Abersoch",
    "Abrantes",
    "Abrenton",
    "Adrano",
    "AeBrey",
    "Aghleam",
    "Akbou",
    "Aldan",
    "Alfaro",
    "Alghero",
    "Almeria",
    "Altnaharra",
    "Ancroft",
    "Anshun",
    "Anstruther",
    "Antor",
    "Arbroath",
    "Arcila",
    "Ardfert",
    "Ardvale",
    "Arezzo",
    "Ariano",
    "Arlon",
    "Avanos",
    "Aveiro",
    "Badalona",
    "Baechahoela",
    "Ballindine",
    "Balta",
    "Banlar",
    "Barika",
    "Bastak",
    "Bayonne",
    "Bejaia",
    "Benlech",
    "Beragh",
    "Bergland",
    "Berneray",
    "Berriedale",
    "Binhai",
    "Birde",
    "Bocholt",
    "Bogmadie",
    "Braga",
    "Brechlin",
    "Brodick",
    "Burscough",
    "Calpio",
    "Canna",
    "Capperwe",
    "Caprera",
    "Carahue",
    "Carbost",
    "Carnforth",
    "Carrigaline",
    "Caserta",
    "Catrianchi",
    "Clatter",
    "Coilaco",
    "Corinth",
    "Corofin",
    "Corran",
    "Corwen",
    "Crail",
    "Cremona",
    "Crieff",
    "Cromarty",
    "Cumbraes",
    "Daingean",
    "Darm",
    "Decca",
    "Derron",
    "Derwent",
    "Deveron",
    "Dezhou",
    "Doedbygd",
    "Doramed",
    "Dornoch",
    "Drammes",
    "Dremmer",
    "Drense",
    "Drimnin",
    "Drumcollogher",
    "Drummore",
    "Dryck",
    "Drymen",
    "Dunbeath",
    "Duncansby",
    "Dunfanaghy",
    "Dunkeld",
    "Dunmanus",
    "Dunster",
    "Durness",
    "Duucshire",
    "Elgomaar",
    "Ellesmere",
    "Ellon",
    "Enfar",
    "Erisort",
    "Eskerfan",
    "Ettrick",
    "Fanders",
    "Farafra",
    "Ferbane",
    "Fetlar",
    "Flock",
    "Florina",
    "Formby",
    "Frainberg",
    "Galloway",
    "Ganzhou",
    "Geal Charn",
    "Gerr",
    "Gifford",
    "Girvan",
    "Glenagallagh",
    "Glenanane",
    "Glin",
    "Glomera",
    "Glormandia",
    "Gluggby",
    "Gnackstein",
    "Gnoelhaala",
    "Golconda",
    "Gourock",
    "Graevbygd",
    "Grandola",
    "Gresberg",
    "Gresir",
    "Greverre",
    "Griminish",
    "Grisbygd",
    "Groddland",
    "Grue",
    "Gurkacre",
    "Haikou",
    "Halkirk",
    "Handan",
    "Hasmerr",
    "Helmsdale",
    "Helmsley",
    "Helsicke",
    "Helvete",
    "Hoersalsveg",
    "Hullevala",
    "Ickellund",
    "Inber",
    "Inverie",
    "Jaca",
    "Jahrom",
    "Jeormel",
    "Jervbygd",
    "Jining",
    "Jotel",
    "Kaddervar",
    "Karand",
    "Karothea",
    "Kashmar",
    "Keswick",
    "Kielder",
    "Killorglin",
    "Kinbrace",
    "Kintore",
    "Kirriemuir",
    "Klen",
    "Knesekt",
    "Kobbe",
    "Komarken",
    "Kovel",
    "Krod",
    "Kursk",
    "Lagos",
    "Lamlash",
    "Langholm",
    "Larache",
    "Larkanth",
    "Larmet",
    "Lautaro",
    "Leighlin",
    "Lervir",
    "Leven",
    "Licata",
    "Limavady",
    "Lingen",
    "Lintan",
    "Liscannor",
    "Locarno",
    "Lochalsh",
    "Lochcarron",
    "Lochinver",
    "Lochmaben",
    "Lom",
    "Lorthalm",
    "Louer",
    "Lurkabo",
    "Luthiir",
    "Lybster",
    "Lynton",
    "Mallaig",
    "Mataro",
    "Melfi",
    "Melvaig",
    "Menter",
    "Methven",
    "Moffat",
    "Monamolin",
    "Monzon",
    "Morella",
    "Morgel",
    "Mortenford",
    "Mullaghcarn",
    "Mulle",
    "Murom",
    "Nairn",
    "Navenby",
    "Nephin Beg",
    "Niskby",
    "Nolle",
    "Nork",
    "Olenek",
    "Oloron",
    "Oranmore",
    "Ormgryte",
    "Orrebygd",
    "Palmi",
    "Panyu",
    "Partry",
    "Pauer",
    "Penhalolen",
    "Perkel",
    "Perski",
    "Planken",
    "Plattland",
    "Pleagne",
    "Pogelveir",
    "Porthcawl",
    "Portimao",
    "Potenza",
    "Praestbygd",
    "Preetsome",
    "Presu",
    "Prettstern",
    "Rantlu",
    "Rappbygd",
    "Rath Luire",
    "Rethel",
    "Riggenthorpe",
    "Rochfort",
    "Roddendor",
    "Roin",
    "Roptille",
    "Roter",
    "Rueve",
    "Sagunto",
    "Saklebille",
    "Salen",
    "Sandwick",
    "Sarab",
    "Sarkanvale",
    "Scandamia",
    "Scarinish",
    "Scourie",
    "Serov",
    "Shanyin",
    "Siegen",
    "Sinan",
    "Sines",
    "Skim",
    "Skokholm",
    "Skomer",
    "Skottskog",
    "Sledmere",
    "Sorisdale",
    "Spakker",
    "Stackforth",
    "Staklesse",
    "Stinchar",
    "Stoer",
    "Strichen",
    "Stroma",
    "Stugslett",
    "Suide",
    "Tabuk",
    "Tarraspan",
    "Tetuan",
    "Thurso",
    "Tiemcen",
    "Tiksi",
    "Tolsta",
    "Toppola",
    "Torridon",
    "Trapani",
    "Tromeforth",
    "Tudela",
    "Turia",
    "Uxelberg",
    "Vaila",
    "Valga",
    "Verguin",
    "Vernlund",
    "Victoria",
    "Waimer",
    "Wett",
    "Xontormia",
    "Yakleks",
    "Yuci",
    "Zaalsehuur",
    "Zamora",
    "Zapulla",
};

//
// The following stuff is just for in this file, to setup the names during
// world setup
//

static int nnames;
static int * nameused;

void SetupNames()
{
    nnames = sizeof regionnames / sizeof (char *);
    nameused = new int[nnames];
    
    for (int i=0; i<nnames; i++) nameused[i] = 0;
}

int AGetName( ARegion *pReg, int town )
{
    int i=0;
    while (i++ < nnames)
    {
        int i = getrandom(nnames);
        if (nameused[i] == 0)
        {
            nameused[i] = 1;
            return i;
        }
    }
    for (i=0; i<nnames; i++) nameused[i] = 0;
    i = getrandom(nnames);
    nameused[i] = 1;
    return i;
}

char *AGetNameString( int name )
{
    return( regionnames[ name ] );
}

char Game::GetRChar(ARegion * r)
{
    int t = r->type;
    char c;
    switch (t) {
    case R_OCEAN:
        return '-';

    case R_PLAIN:
        c = 'p';
        break;
    case R_FOREST:
        c = 'f';
        break;
    case R_MOUNTAIN:
        c = 'm';
        break;
    case R_SWAMP:
        c = 's';
        break;
    default:
        return '?';

    }
    
    if (r->town) {
        c = (c - 'a') + 'A';
    }
    return c;
}

void Game::CreateWorld()
{
    int nPlayers = 0;
    while( nPlayers <= 0 )
    {
        Awrite( "How many players will be in this game? ");
        nPlayers = Agetint();
    }

    regions.CreateLevels( 1 );

    SetupNames();

    regions.CreateIslandLevel( 0, nPlayers, 0 );

    regions.CalcDensities();
}

void Game::CreateNPCFactions()
{
    // xxxxx
    guardfaction = -1;
    monfaction = -1;
}

void Game::CreateCityMon( ARegion *pReg, int percent )
{
    //
    // No city monsters
    //
}

void Game::AdjustCityMons( ARegion *r )
{
    //
    // No city monsters.
    //
}

void Game::AdjustCityMon( ARegion *r, Unit *u )
{
    //
    // No city monsters.
    //
}

int Game::MakeWMon( ARegion *pReg )
{
    //
    // No wandering monsters
    //
    return( 0 );
}

void Game::MakeLMon( Object *pObj )
{
    //
    // No lair monsters.
    //
}

int ARegionList::GetRegType( ARegion *pReg )
{
    ARegionArray *pRA = pRegionArrays[ pReg->zloc ];
    int xBase = 0;
    int yBase = 0;
    int isIsland = 0;
    if( pReg->xloc < 8 )
    {
        isIsland = 1;
        xBase = pReg->xloc - 2;
        yBase = ( pReg->yloc - 10 ) % 6;
    }

    if( pReg->yloc < 8 )
    {
        isIsland = 1;
        xBase = ( pReg->xloc - 10 ) % 6;
        yBase = pReg->yloc - 2;
    }

    if( pReg->xloc >= pRA->x - 8 )
    {
        isIsland = 1;
        xBase = pReg->xloc + 6 - pRA->x;
        yBase = ( pReg->yloc - 10 ) % 6;
    }

    if( pReg->yloc >= pRA->y - 8 )
    {
        isIsland = 1;
        xBase = ( pReg->xloc - 10 ) % 6;
        yBase = pReg->yloc + 6 - pRA->y;
    }

    if( isIsland )
    {
        if( xBase == 0 || xBase == 3 )
        {
            return( R_SWAMP );
        }

        if( yBase == 0 || yBase == 3 )
        {
            return( R_SWAMP );
        }

        if( xBase == 1 && yBase == 1 )
        {
            return( R_MOUNTAIN );
        }

        if( xBase == 2 && yBase == 2 )
        {
            return( R_PLAIN );
        }

        //
        // This shouldn't get called
        //
        return( R_OCEAN );
    }

    int r = getrandom(5);
    switch (r)
    {
    case 0:
    case 1:
        return R_PLAIN;
    case 2:
        return R_FOREST;
    case 3:
        return R_MOUNTAIN;
    case 4:
        return R_SWAMP;
    }

    //
    // This really shouldn't get called
    //
    return( R_OCEAN );
}

int ARegionList::CheckRegionExit( int nDir, ARegion *pFrom, ARegion *pTo )
{
    return( 1 );
}

int ARegionList::GetWeather( ARegion *pReg, int month )
{
    //
    // No weather for this game.
    //
    return( W_NORMAL );
}

int ARegion::CanBeStartingCity( ARegionArray *pRA )
{
    //
    // If the region is in the middle of the map, it isn't on an island,
    // and cannot be a starting region.
    //
    if( xloc >= 8 &&
        yloc >= 8 &&
        xloc < pRA->x - 8 &&
        yloc < pRA->y - 8 )
    {
        return( 0 );
    }

    //
    // If the region is not a plain, it cannot be a starting region.
    //
    if( type != R_PLAIN )
    {
        return( 0 );
    }

    return( 1 );
}

void ARegion::MakeStartingCity() 
{
    //
    // Not used.
    //
}

int ARegion::IsStartingCity()
{
    forlist(&objects) {
        Object *o = (Object *) elem;
        if( o->type == O_CITADEL )
        {
            return( 1 );
        }
    }

    return( 0 );
}

int ARegion::IsSafeRegion()
{
    return( 0 );
}

ARegion *ARegionList::GetStartingCity( ARegion *AC,
                                       int i,
                                       int level,
                                       int maxX,
                                       int maxY )
{
    //
    // Not used.
    //

    return( 0 );
}


