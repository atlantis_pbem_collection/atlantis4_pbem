// START A3HEADER
//
// This source file is part of the Atlantis PBM game program.
// Copyright (C) 1995-1999 Geoff Dunbar
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program, in the file license.txt. If not, write
// to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
// Boston, MA 02111-1307, USA.
//
// See the Atlantis Project web page for details:
// http://www.prankster.com/project
//
// END A3HEADER
#include "game.h"
#include "rules.h"

void Game::ProcessCastOrder(Unit * u,AString * o, OrdersCheck *pCheck )
{
    AString * token = o->gettoken();
    if (!token)
    {
        ParseError( pCheck, u, 0, "CAST: No skill given.");
        return;
    }

    int sk = ParseSkill(token);
    delete token;
    if (sk==-1)
    {
        ParseError( pCheck, u, 0, "CAST: Invalid skill.");
        return;
    }

    if( !( SkillDefs[sk].flags & SkillType::MAGIC ))
    {
        ParseError( pCheck, u, 0, "CAST: That is not a magic skill.");
        return;
    }
    if( !( SkillDefs[sk].flags & SkillType::CAST ))
    {
        ParseError( pCheck, u, 0, "CAST: That skill cannot be CAST.");
        return;
    }

    if( !pCheck )
    {
        //
        // xxxxx - should be error checking spells
        //
        switch(sk) {
        case S_CREATE_STAFF_OF_LIGHT:
        case S_CREATE_STAFF_OF_FEAR:
        case S_CREATE_STAFF_OF_FIRE:
        case S_CREATE_AMULET_OF_INVISIBILITY:
        case S_CREATE_AMULET_OF_TRUE_SEEING:
            ProcessCreateSpell( u, sk, pCheck );
            break;
        case S_TELEPORT:
            ProcessTeleport( u, o, pCheck );
        }
    }
}

void Game::ProcessCreateSpell( Unit *pUnit, int skill, OrdersCheck *pCheck )
{
    pUnit->ClearCastOrders();
    CastOrder *orders = new CastOrder;
    orders->spell = skill;
    orders->level = 1;
    pUnit->castorders = orders;
}

void Game::ProcessTeleport( Unit *pUnit, 
                            AString *pOrders,
                            OrdersCheck *pCheck )
{
    AString *token = pOrders->gettoken();
    if (!token)
    {
        pUnit->Error("CAST: Requires a target mage.");
        return;
    }
    int gate = token->value();

    delete token;
    token = pOrders->gettoken();

    if (!token)
    {
        pUnit->Error("CAST: No units to teleport.");
        return;
    }

    if (!(*token == "units"))
    {
        pUnit->Error("CAST: No units to teleport.");
        delete token;
        return;
    }

    TeleportOrder *order;

    if( pUnit->teleportorders && pUnit->teleportorders->spell == S_TELEPORT )
    {
        order = pUnit->teleportorders;
    } 
    else
    {
        order = new TeleportOrder;
        pUnit->ClearCastOrders();
        pUnit->teleportorders = order;
    }

    order->gate = gate;
    order->spell = S_TELEPORT;
    order->level = 1;

    UnitId *id = ParseUnit( pOrders );
    while(id)
    {
        order->units.Add(id);
        id = ParseUnit( pOrders );
    }
}

void Game::RunACastOrder(ARegion * r,Object *o,Unit * u)
{
    if (u->type != U_MAGE)
    {
        u->Error("CAST: Unit is not a mage.");
        return;
    }

    switch( u->castorders->spell )
    {
    case S_CREATE_STAFF_OF_LIGHT:
        RunCreateItem( u, S_CREATE_STAFF_OF_LIGHT, I_STAFF_OF_LIGHT, 200 );
        break;
    case S_CREATE_STAFF_OF_FEAR:
        RunCreateItem( u, S_CREATE_STAFF_OF_FEAR, I_STAFF_OF_FEAR, 400 );
        break;
    case S_CREATE_STAFF_OF_FIRE:
        RunCreateItem( u, S_CREATE_STAFF_OF_FIRE, I_STAFF_OF_FIRE, 600 );
        break;
    case S_CREATE_AMULET_OF_INVISIBILITY:
        RunCreateItem( u, S_CREATE_AMULET_OF_INVISIBILITY,
                       I_AMULET_OF_INVISIBILITY, 600 );
        break;
    case S_CREATE_AMULET_OF_TRUE_SEEING:
        RunCreateItem( u, S_CREATE_AMULET_OF_TRUE_SEEING,
                       I_AMULET_OF_TRUE_SEEING, 600 );
        break;
    }
}

void Game::RunCreateItem( Unit *pUnit, int skill, int item, int cost )
{
    int level = pUnit->GetSkill(skill);

    if( pUnit->items.GetNum(I_SILVER) < cost )
    {
        pUnit->Error("Doesn't have enough silver to create that.");
        return;
    }

    pUnit->items.SetNum( I_SILVER, 
                         pUnit->items.GetNum( I_SILVER ) - cost );

    if( getrandom( 5 ) >= level )
    {
        pUnit->Event( AString( "Fails to create " ) + ItemString( item, 1 ) +
                  "." );
    }
    else
    {
        pUnit->items.SetNum( item, pUnit->items.GetNum( item ) + 1 );
        pUnit->Event( AString( "Creates " ) + ItemString( item, 1 ) + "." );
    }
}

void Game::RunTeleportOrders()
{
    forlist(&regions) {
        ARegion * r = (ARegion *) elem;
        forlist(&r->objects) {
            Object * o = (Object *) elem;
            int foundone = 1;
            while (foundone)
            {
                foundone = 0;
                forlist(&o->units) {
                    Unit *u = (Unit *) elem;
                    if (u->teleportorders)
                    {
                        RunTeleport( u );
                        delete u->teleportorders;
                        u->teleportorders = 0;
                        break;
                    }
                }
            }
        }
    }
    
}

void Game::RunTeleport( Unit *pUnit )
{
    int level = pUnit->GetSkill( S_TELEPORT );
    TeleportOrder *pOrder = pUnit->teleportorders;

    if (!level)
    {
        pUnit->Error("CAST: Doesn't know Teleport.");
        return;
    }

    ARegion *pSrc = pUnit->object->region;

    int maxweight = 1000;
    int maxdist = 5 * level;
    int weight = 0;
    forlist( &( pOrder->units )) {
        Unit *pTaru = pSrc->GetUnitId( (UnitId *) elem, pUnit->faction->num );
        if (pTaru) weight += pTaru->Weight();
    }

    if (weight > maxweight)
    {
        pUnit->Error( "CAST: That mage cannot teleport that much weight." );
        return;
    }

    Location *pTar = regions.FindUnit( pOrder->gate );
    if (!pTar)
    {
        pUnit->Error("CAST: No such target mage.");
        return;
    }

    if( pTar->unit->faction->GetAttitude( pUnit->faction->num ) < A_FRIENDLY )
    {
        pUnit->Error("CAST: Target mage is not friendly.");
        return;
    }

    if ( pTar->unit->type != U_MAGE )
    {
        pUnit->Error("CAST: Target is not a mage.");
        return;
    }

    if (!pTar->unit->GetSkill( S_TELEPORT ))
    {
        pUnit->Error( "CAST: Target does not know the Teleport spell." );
        return;
    }

    if( regions.GetDistance( pSrc, pTar->region ) > maxdist )
    {
        pUnit->Error("CAST: Can't Portal Jump that far.");
        return;
    }

    pUnit->Event("Casts Teleport.");

    {
        forlist( &( pOrder->units )) {
            Location *loc = pSrc->GetLocation( (UnitId *) elem,
                                               pUnit->faction->num );
            if (loc) 
            {
                if (loc->unit->GetAttitude( pSrc, pUnit ) < A_ALLY) 
                {
                    pUnit->Error("CAST: Unit is not allied.");
                } 
                else
                {
                    loc->unit->Event(AString("Is teleported to ") +
                                     pTar->region->Print( &regions ) + " by " +
                                     *pUnit->name + ".");
                    loc->unit->MoveUnit( pTar->obj );
                    if (loc->unit != pUnit) loc->unit->ClearCastOrders();
                }
                delete loc;
            } 
            else
            {
                pUnit->Error("CAST: No such unit.");
            }
        }
    }
    
    delete pTar;
}

